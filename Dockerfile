FROM php:8-apache

ENV APACHE_DOCUMENT_ROOT /var/www/html
ENV TZ America/Mexico_City

# Instalar extensiones PHP necesarias
RUN apt-get update && apt-get install -y  git \
                        libfreetype6-dev \
                        libjpeg62-turbo-dev \
                        zlib1g-dev \
                        libpng-dev \
                        libzip-dev \ 
			libxml2-dev \
			unzip 

RUN docker-php-ext-install  mysqli \
                            gd \
                            zip \
                            pdo \
                            pdo_mysql \
                            soap

RUN a2enmod rewrite

RUN service apache2 restart
